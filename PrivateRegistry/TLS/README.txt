Note: While creating minikube cluster,we need to pass insecure-registry.It only applicable while cluster
      creating.If not then guest port will not map with host port.

openssl req -x509 -newkey rsa:4096 -days 100 -nodes -config openssl.conf -keyout certs/domain.key -out certs/domain.crt

openssl x509 -text -noout -in certs/domain.crt

vi /etc/docker/daemon.json
{
 "insecure-registries":["192.100.100.1:5000"]
}
sudo systemctl restart docker
minikube start -p p1 --driver=virtualbox --insecure-registry=192.168.99.1:5000
